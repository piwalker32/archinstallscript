#!/usr/bin/env bash

# Install dependencies

pacman -Sy --needed util-linux dialog ncurses # add new deps here

# Configuration

dialog --msgbox "This script will configure and install Arch Linux on your system." 0 0

## Partitioning

exec 3>&1
dialog --title "Partitioning" --default-button "no" --no-label "BIOS" --yes-label "UEFI" --yesno "How would you like to install the boot loader?" 0 0
bios=$?
set -e
method=$(dialog --title "Partioning" --menu "Choose a Partitioning Method" 0 0 0 0 "Auto" 1 "Auto - No Swap" 2 "Manual" 2>&1 1>&3)
disks=$(fdisk -l 2>/dev/null |awk 'BEGIN{count=0} /^Disk \//{print count++,substr($2,0,length($2)-1)}')

case $method in
    0)
        selection=$(dialog --title "Partitioning" --menu "Chose disk (Warning: This will erase *all* data on the disk!)" 0 0 0 $disks 2>&1 1>&3)
        disk=$(fdisk -l 2>/dev/null | awk '/^Disk \// && n++ == '${selection}'{print substr($2,0,length($2)-1)}')
        if [ $bios -eq 1 ]; then
            sed -e 's/\s*\([\+0-9a-zA-Z]*\).*/\1/' << EOF | fdisk $disk
                o # clear the in memory partition table
                n # new partition
                p # primary partition
                1 # partition number 1
                  # default - start at beginning of disk
                +5G # 5 GB swap parttion
                n # new partition
                p # primary partition
                2 # partion number 2
                  # default, start immediately after preceding partition
                  # default, extend partition to end of disk
                a # make a partition bootable
                2 # bootable partition is partition 2 -- /dev/xxx2
                p # print the in-memory partition table
                w # write the partition table
                q # and we're done
EOF
            sleep 1
            mkswap ${disk}1
            mkfs.btrfs -f ${disk}2
            mount ${disk}2 /mnt
            swapon ${disk}1
            btrfs subvolume create /mnt/@
            btrfs subvolume create /mnt/@home
            umount /mnt
            mount ${disk}2 /mnt -o 'subvol=/@'
            mkdir /mnt/home
            mkdir -p /mnt/media/root-vol
            mount ${disk}2 /mnt/home -o 'subvol=/@home'
            mount ${disk}2 /mnt/media/root-vol -o 'subvol=/'
        else
            sed -e 's/\s*\([\+0-9a-zA-Z]*\).*/\1/' << EOF | fdisk $disk
                g # clear the in memory partition table
                n # new partition
                1 # partition number 1
                  # default - start at beginning of disk
                +500M # 500 MB boot parttion
                t # change partition type
                EFI System # EFI partition type
                n # new partition
                2 # partion number 2
                  # default, start immediately after preceding partition
                +5G # 5 GB swap partition
                n # new partition
                3 # partition number 3
                  # default, start immediately after preceding partition
                  # default, use the rest of the disk
                p # print the in-memory partition table
                w # write the partition table
                q # and we're done
EOF
            sleep 1
            mkswap ${disk}2
            mkfs.btrfs -f ${disk}3
            mkfs.fat -F 32 ${disk}1
            mount ${disk}3 /mnt
            swapon ${disk}2
            btrfs subvolume create /mnt/@
            btrfs subvolume create /mnt/@home
            umount /mnt
            mount ${disk}3 /mnt -o 'subvol=/@'
            mkdir /mnt/home
            mkdir -p /mnt/media/root-vol
            mkdir /mnt/boot
            mount ${disk}3 /mnt/home -o 'subvol=/@home'
            mount ${disk}3 /mnt/media/root-vol -o 'subvol=/'
            mount ${disk}1 /mnt/boot

        fi
        ;;
    1)
        selection=$(dialog --title "Partitioning" --menu "Chose disk (Warning: This will erase *all* data on the disk!)" 0 0 0 $disks 2>&1 1>&3)
        disk=$(fdisk -l 2>/dev/null | awk '/^Disk \// && n++ == '${selection}'{print substr($2,0,length($2)-1)}')
        if [ $bios -eq 1 ]; then
            sed -e 's/\s*\([\+0-9a-zA-Z]*\).*/\1/' << EOF | fdisk $disk
                o # clear the in memory partition table
                n # new partition
                p # primary partition
                1 # partition number 1
                  # default - start at beginning of disk
                  # default, extend partition to end of disk
                a # make a partition bootable
                1 # bootable partition is partition 2 -- /dev/xxx1
                p # print the in-memory partition table
                w # write the partition table
                q # and we're done
EOF
            sleep 1
            mkfs.btrfs -f ${disk}1
            mount ${disk}1 /mnt
            btrfs subvolume create /mnt/@
            btrfs subvolume create /mnt/@home
            umount /mnt
            mount ${disk}1 /mnt -o 'subvol=/@'
            mkdir /mnt/home
            mkdir -p /mnt/media/root-vol
            mount ${disk}1 /mnt/home -o 'subvol=/@home'
            mount ${disk}1 /mnt/media/root-vol -o 'subvol=/'
        else
            sed -e 's/\s*\([\+0-9a-zA-Z]*\).*/\1/' << EOF | fdisk $disk
                g # clear the in memory partition table
                n # new partition
                1 # partition number 1
                  # default - start at beginning of disk
                +500M # 500 MB boot parttion
                t # change partition type
                EFI System # EFI partition type
                n # new partition
                2 # partion number 2
                  # default, start immediately after preceding partition
                  # default, use the rest of the disk
                p # print the in-memory partition table
                w # write the partition table
                q # and we're done
EOF
            sleep 1
            mkfs.btrfs -f ${disk}2
            mkfs.fat -F 32 ${disk}1
            mount ${disk}2 /mnt
            btrfs subvolume create /mnt/@
            btrfs subvolume create /mnt/@home
            umount /mnt
            mount ${disk}2 /mnt -o 'subvol=/@'
            mkdir /mnt/home
            mkdir -p /mnt/media/root-vol
            mkdir /mnt/boot
            mount ${disk}2 /mnt/home -o 'subvol=/@home'
            mount ${disk}2 /mnt/media/root-vol -o 'subvol=/'
            mount ${disk}1 /mnt/boot

        fi
        ;;
    2)
        dialog --msgbox "Manual Partition." 0 0 2>&1 1>&3
        ;;
esac




## dialog cleanup

exec 3>&-
